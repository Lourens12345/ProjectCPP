#pragma once

#include <math.h>

#include "Vec2.h"
#include "Vec3.h"
#include "Vec4.h"
#include "Mat4.h"

namespace maths
{

	inline float toRadians(float degrees)
	{
		return degrees * (M_PI / 180.0f);
	}

}