#include "Vec3.h"

namespace maths
{
	Vec3::Vec3()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	Vec3::Vec3(const float & x, const float & y, const float & z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vec3 & maths::Vec3::Add(const Vec3 & other)
	{
		x += other.x;
		y += other.y;
		z += other.z;

		return *this;
	}

	Vec3 & maths::Vec3::Subtract(const Vec3 & other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;

		return *this;
	}

	Vec3 & maths::Vec3::Multiply(const Vec3 & other)
	{
		x *= other.x;
		y *= other.y;
		z *= other.z;

		return *this;
	}

	Vec3 & maths::Vec3::Divide(const Vec3 & other)
	{
		x /= other.x;
		y /= other.y;
		z /= other.z;

		return *this;
	}

	Vec3 maths::operator+(Vec3 left, const Vec3 & right)
	{
		return left.Add(right);
	}

	Vec3 maths::operator-(Vec3 left, const Vec3 & right)
	{
		return left.Subtract(right);
	}

	Vec3 maths::operator*(Vec3 left, const Vec3 & right)
	{
		return left.Multiply(right);
	}

	Vec3 maths::operator/(Vec3 left, const Vec3 & right)
	{
		return left.Divide(right);
	}

	bool maths::Vec3::operator==(const Vec3 & other)
	{
		return x == other.x && y == other.y && z == other.z;
	}

	bool maths::Vec3::operator!=(const Vec3 & other)
	{
		return x != other.x || y != other.y || z != other.z;
	}

	Vec3 Vec3::operator+=(const Vec3 & other)
	{
		return Add(other);
	}

	Vec3 Vec3::operator-=(const Vec3 & other)
	{
		return Subtract(other);
	}

	Vec3 Vec3::operator*=(const Vec3 & other)
	{
		return Multiply(other);
	}

	Vec3 Vec3::operator/=(const Vec3 & other)
	{
		return Divide(other);
	}

	std::ostream & maths::operator<<(std::ostream & stream, const Vec3 & vector)
	{
		stream << "Vec3: (" << vector.x << ", " << vector.y << ", " << vector.z << ")";
		return stream;
	}
}