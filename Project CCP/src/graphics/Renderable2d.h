#pragma once

#include "buffers/Buffer.h"
#include "buffers/VertexArray.h"
#include "buffers/IndexBuffer.h"
#include "../maths/maths.h"
#include "Shader.h"

namespace graphics
{

	struct VertexData
	{
		maths::Vec3 vertex;
		maths::Vec4 color;
	};

	class Renderable2d
	{
	protected:
		maths::Vec3 m_Position;
		maths::Vec2 m_Size;
		maths::Vec4 m_Color;

		
	public:
		Renderable2d(maths::Vec3 position, maths::Vec2 size, maths::Vec4 color)
			: m_Position(position), m_Size(size), m_Color(color)
		{}

		virtual ~Renderable2d() {}

		inline maths::Vec3& GetPosition() { return m_Position; }
		inline maths::Vec2& GetSize() { return m_Size; }
		inline maths::Vec4& GetColor() { return m_Color; }
	};

}