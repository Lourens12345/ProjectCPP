#include "Sprite.h"

namespace graphics
{

	Sprite::Sprite(float x, float y, float width, float height, maths::Vec4 color)
		: Renderable2d(maths::Vec3(x, y, 0), maths::Vec2(width, height), color)
	{}

}