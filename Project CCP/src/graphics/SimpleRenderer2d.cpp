#include "SimpleRenderer2d.h"

namespace graphics
{

	void graphics::SimpleRenderer2d::submit(Renderable2d* renderable)
	{
		m_RenderQueue.push_back((StaticSprite*) renderable);
	}

	void graphics::SimpleRenderer2d::flush()
	{
		while (!m_RenderQueue.empty())
		{
			StaticSprite* sprite = m_RenderQueue.front();
			sprite->GetVAO()->bind();
			sprite->GetIBO()->bind();

			sprite->GetShader().setUniformMat4("ml_matrix", maths::Mat4::translation(sprite->GetPosition()));
			glDrawElements(GL_TRIANGLES, sprite->GetIBO()->getCount(), GL_UNSIGNED_SHORT, 0);

			sprite->GetIBO()->unbind();
			sprite->GetVAO()->unbind();

			m_RenderQueue.pop_front();
		}
	}

}