#pragma once

#include <GL\glew.h>
#include "../maths/maths.h"
#include "Renderable2d.h"

namespace graphics
{

	class Renderer2d
	{
	public:
		virtual void submit(Renderable2d* renderable) = 0;
		virtual void flush() = 0;
	};

}