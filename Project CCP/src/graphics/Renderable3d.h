#pragma once

#include <GL\glew.h>
#include <vector>

#include "../maths/maths.h"

namespace graphics
{

	class Renderable3d
	{
	private:
		std::vector<maths::Vec4> m_Vertices;
		std::vector<GLushort> m_Indices;
	public:
		Renderable3d();
		~Renderable3d();
	};

}