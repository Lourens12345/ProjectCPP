#pragma once

#include <GL\glew.h>
#include <vector>

#include "../Utils/FileUtils.h"
#include "../maths/maths.h"

namespace graphics
{
	class Shader
	{
		
	private:
		GLuint m_ShaderID;
		const char *m_VertPath, *m_FragPath;
	public:
		Shader(const char* vertPath, const char* fragPath);
		~Shader();

		void setUniform1i(const GLchar* name, int value);
		void setUniform1f(const GLchar* name, float value);
		void setUniform2f(const GLchar* name, maths::Vec2& value);
		void setUniform3f(const GLchar* name, maths::Vec3& value);
		void setUniform4f(const GLchar* name, maths::Vec4& value);
		void setUniformMat4(const GLchar* name, const maths::Mat4& matrix);

		void enable() const;
		void disable() const;
	private:
		GLuint load();
		GLint getUniformLocation(const GLchar* name);
	};
}