#pragma once

#include "Renderable2d.h"

namespace graphics
{

	class Sprite : public Renderable2d
	{
	private:
	public:
		Sprite(float x, float y, float width, float height, maths::Vec4 color);
		
	};

}