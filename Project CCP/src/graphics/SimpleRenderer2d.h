#pragma once

#include <deque>

#include "renderer2d.h"
#include "StaticSprite.h"

namespace graphics
{

	class SimpleRenderer2d : public Renderer2d
	{
	private:
		std::deque<StaticSprite*> m_RenderQueue;
	public:
		void submit(Renderable2d* renderable) override;
		void flush() override;
	};

}
