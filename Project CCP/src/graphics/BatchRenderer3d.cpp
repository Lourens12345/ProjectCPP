#include "BatchRenderer3d.h"

namespace graphics
{

	BatchRenderer3d::BatchRenderer3d()
	{
		init();
	}

	BatchRenderer3d::~BatchRenderer3d()
	{
		delete m_IBO;
		glDeleteBuffers(1, &m_VBO);
	}

	void BatchRenderer3d::init()
	{
		glGenVertexArrays(1, &m_VAO);
		glGenBuffers(1, &m_VBO);

		glBindVertexArray(m_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferData(GL_ARRAY_BUFFER, RENDERER_BUFFER_SIZE, 0, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(SHADER_VERTEX_INDEX);
		glEnableVertexAttribArray(SHADER_COLOR_INDEX);
		glVertexAttribPointer(SHADER_VERTEX_INDEX, 3, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)0);
		glVertexAttribPointer(SHADER_COLOR_INDEX, 4, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)(3 * sizeof(GLfloat)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		GLuint* indices = 0; // TODO: Add memory address of indices.
		m_IndexCount = 0; // TODO: Add amount of indices here.

		m_IBO = new IndexBuffer(indices, RENDERER_INDICES_SIZE);
		delete indices;
		glBindVertexArray(0);
	}

	void BatchRenderer3d::begin()
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		m_Buffer = (VertexData*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	}

	void BatchRenderer3d::submit(Renderable2d * renderable)
	{
		
	}

	void BatchRenderer3d::end()
	{
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}

	void BatchRenderer3d::flush()
	{
		glBindVertexArray(m_VAO);
		m_IBO->bind();

		glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_INT, 0);

		m_IBO->unbind();
		glBindVertexArray(0);
	}

}