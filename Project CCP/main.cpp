#include "src/graphics/window.h"
#include "src/maths/maths.h"

#include "src/graphics/Shader.h"
#include "src/graphics/buffers/Buffer.h"
#include "src/graphics/buffers/IndexBuffer.h"
#include "src/graphics/buffers/VertexArray.h"

#include "src/graphics/Renderer2d.h"
#include "src/graphics/SimpleRenderer2d.h"
#include "src/graphics/BatchRenderer2d.h"

#include "src/graphics/StaticSprite.h"
#include "src/graphics/Sprite.h"

#include <time.h>

int main() 
{
	using namespace graphics;
	using namespace maths;

	Window window("Project CPP", 960, 540);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Mat4 ortho = Mat4::orthographic(0.0f, 16.0f, 0.0f, 9.0f, -1.0f, 1.0f);
	Shader shader("src/shaders/basic.vert", "src/shaders/basic.frag");
	shader.enable();
	shader.setUniformMat4("pr_matrix", ortho);

	std::vector<Renderable2d*> sprites;

	srand(time(NULL));
	
	for (float y = 0; y < 9.0f; y += 0.05f) 
	{
		for (float x = 0; x < 16.0f; x += 0.05f )
		{
			sprites.push_back(new Sprite(x, y, 0.04f, 0.04f, maths::Vec4(rand() % 1000 / 1000.0f, 0, 1, 1)));
		}
	}

	BatchRenderer2d renderer;

	while (!window.closed()) 
	{
		window.clear();

		double x, y;
		window.getMousePosition(x, y);
		Vec2 lightPos((float)(x * 16.0f / 960.0f), (float)(9.0f - y * 9.0f / 540.0f));
		shader.setUniform2f("light_pos", lightPos);
		renderer.begin();
		for (int i = 0; i < sprites.size(); i++)
		{
			renderer.submit(sprites[i]);
		}
		renderer.end();
		renderer.flush();

		window.update();
	}

	//system("PAUSE");
	return 0;

}